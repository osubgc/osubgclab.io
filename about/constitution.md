---
layout: default-with-toc
title: Buckeye Gaming Collective Constitution
---

## Buckeye Gaming Collective Constitution

### Article I - Name, Purpose, and Non-Discrimination Policy of the Organization

#### Section 1 - Name

Buckeye Gaming Collective (BGC)

#### Section 2 - Purpose

The purpose of Buckeye Gaming Collective is to galvanize recreational and competitive video game players, advance the local gaming scene, foster a community for students of the Ohio State University interested in gaming, pursue and solidify collegiate esports at the University in conjunction with The Ohio State University, and perform philanthropy and charity work for local causes and organizations related to gaming such as Extra Life.

#### Section 3 - What We Do

Buckeye Gaming Collective hosts casual and competitive gaming events such as tournaments, LANs, and viewing parties, as well as manage and support competitive esport teams in order to foster growth in the collegiate esports scene. We host and participate in local charity events in order to beneficially impact the community and maintain a positive image of the industry and its consumers. We also assist those wanting to work in the video game industry by fostering connections with employers and providing the experiences and training required to develop essential skills for working within the industry.

#### Section 4 - Non-Discrimination Policy

This organization and its members shall not discriminate against any individual(s) for reasons of age, color, disability, gender identity or expression, national origin, ethnicity/race, religion, sex, sexual orientation, veteran status, and/or skill-level.

### Article II - Membership: Qualifications and Categories of Membership

#### Section 1 - Categories of Membership

##### General Members

- General Members are full-time undergraduate or graduate students at The Ohio State University.
- General Members are permitted to attend any and all events run by the organization.
- General Members are not permitted to vote in formal elections.
- General Members must follow an inclusive environment for all ages, colors, disabilities, gender identities or expressions, national origin, ethnicities/races, religions, sexes, sexual orientations, veteran status, and/or skill-levels.

##### Voting Members

- Voting Members are those who meet the General Member requirements and follow these additional requirements:
- Attend monthly General Body Meetings with two or less unexcused absences per semester
- Assist with one fundraiser event a semester OR pay $5 a semester
- Be a part of at least one committee or hold a Minor Board or Executive Board position

##### Alum Members

- Alum Members are graduates of The Ohio State University who were General and/or Voting Members when they were full-time, students at the University.
- Alum Members may not hold Executive Board positions. Vacancies in Minor Board positions may be filled by an Alum Member only if an exception is granted by two-thirds vote of the Executive Board.
- Alum Members are encouraged to stay in contact with the club through the Director of Public Relations and Diversity, attend community events, and attend any alumni events held.
- Alum Members must follow an inclusive environment for all ages, colors, disabilities, gender identities or expressions, national origin, ethnicities/races, religions, sexes, sexual orientations, veteran status, and/or skill-levels.

#### Section 2 - Executive Board, Minor Board, and Terms of Office

The Executive Board shall be made up of six Officers, the President, Treasurer, Director of Public Relations & Diversity, Director of Community Management, Director of Competitive Affairs, and Director of Sponsorships. The Executive Board shall represent the entire organization and its general membership. These Officers are elected from the ranks of the organization’s voting membership. Term length for any and all Officers shall be limited to one year post-election. Officers are eligible for re-election the subsequent year, granted that they continue to be enrolled as an undergraduate, full-time student in good standing with the University. All Executive Officers are required to attend an Inclusive Leadership Training seminar once elected. All Executive Board members must have been Voting Members for a least one year, or have previously been on the Minor Board.

The Minor Board shall be made up of the Community Game Managers, Competitive Game Managers, Broadcast Chair, Social Media Chair, Graphic Design Chair and Philanthropy Chair. The Minor Board will be selected by a two-thirds vote by the Executive Board and with terms lasting one year post-selection. All Minor Board positions are eligible for re-selection the subsequent year, provided they continue to be enrolled as an undergraduate, full-time student with and in good standing with the University.

Committees are to consist of any Voting, General, or Alum Members of the Club, or any other Club connections, including Graduate students or respected members of the public.  Committees may be created or dissolved at any time as the Executive Board sees fit.  Members of these Committees will be decided by the Director and/or Chair presiding over them. Committees will not be discussed ad nauseum in this document.

#### Section 3 - Titles and Duties of Leaders

##### Committees

Committees shall consist of any Members of the club (General, Voting, and/or Alum) and may be created or disbanded by the Executive Board as they see fit.

##### Minor Board

###### Broadcast Chair (Management)

- Coordinate with the Executive Board to film and stream events.
- Coordinate broadcasts and other content creation.
- Manage the video content creation team including casters, broadcasters, videographers, editors, streamers and any other members.
- Work with the Director of Public Relations to establish guidelines for all broadcasts, video content, and individual streams so that the brand is represented consistently.

###### Broadcast Chair (Technology)

- Organize audiovisual technology for streaming.
- Know and understand how to run an effective broadcast.
- Be the primary consult go-to for all broadcast matters.
- Work with the Director of Public Relations to enforce guidelines for all broadcasts so that the brand is represented consistently.

###### Social Media Chair

- Manage the network of social media platforms in use by Ohio State Esports Initiative, such as Twitter, Instagram, Twitch, Facebook, etc.
- Coordinate content schedules for competitive, community, and broadcast events with the Director of Public Relations and Diversity.
- Coordinate all necessary design content for social pages with the Graphic Design chair.
- Answer all necessary social media messages and forward all important information to the Director of Public Relations and Diversity.
- Track engagement and activity and present findings to the Director of Public Relations and Diversity regularly.

###### Graphic Design Chair

- Serve as the head of graphic design production for use by the Buckeye Gaming Collective.
- Follow content schedules created by the Director of Public Relations and Diversity.
- Communicate all necessary design content with the Social Media and Broadcast Chairs.
- Fully uphold artist integrity for all work.

###### Philanthropy Chair

- Work with the Director of Public Relations and Diversity to seek philanthropic opportunities for the Buckeye Gaming Collective.
- Monitor the organization’s service hours and fundraising amounts, as well as submit them to the necessary mediums/organizations.
- Set up the organization's Extra Life charity page at the beginning of every calendar year.
- Act as the secretary for all Public Relations Branch Meetings, whose responsibilities include creating meeting notes summarizing ideas, discussion points, and decisions made during the branch meeting.
- Recording active members at events and report list to Treasurer
- Report improper use of club funds to Faculty Advisor.

###### Competitive Team Managers - One for each game

- Coordinate with the Executive Board to coordinate necessary team functions, including tryouts, practices, scrimmages, and matches for their respective team.
- Coordinate with the Executive Board to ensure events and travel are properly organized and funded.
- Work with Community Managers to unify players under Buckeye Gaming Collective (BGC).
- Manage team practices, games, and league involvement.
- Are encouraged to enlist the help of statisticians, analysts, and coaches to provide their team as many resources as possible.

###### Secretary of Competitive Management

- Keep an up-to-date profile of club actions relating to competitive events.
- Create and update all necessary calendars with competitive event details.
- Create Competitive Branch Meeting notes for all members that includes a summary of ideas, discussion points, and decisions made during the Branch meetings.
- Coordinate with the Director of Public Relations and Diversity to send emails to all members notifying them of upcoming match dates and times.

###### Community Managers

- Coordinate with the Executive Board to plan and execute events, including room bookings and organizing, marketing, and funding the events.
- Responsible for tracking community engagement and opinions about events.
- Be able to work independently on planning and executing their events.
- Work with Competitive Managers to unify players under Buckeye Gaming Collective (BGC).
- Encouraged to collaborate with other managers on events.

###### Secretary of Community Management

- Keep an up-to-date profile of club actions relating to community events.
- Create and update all necessary calendars with community event details.
- Create Community Branch meeting notes for all members that includes a summary of ideas, discussion points, and decisions made during the meetings.
- Coordinate with the Director of Public Relations and Diversity to send emails to all members notifying them of upcoming event dates and times.

###### All Members of the Minor Board shall

- Be a friendly and positive force in the club community.
- Adhere to all responsibilities and duties set forth by the Constitution.
- Be a Voting Member of the Buckeye Gaming Collective.
- Communicate in weekly Branch Meetings with their respective Director, secretary, and other necessary staff persons with updates and information.
- Attend all General Body and Branch Meetings unless there is an excused absence, with their Director keeping them accountable for absences.
- Manage their own respective committees, delegating tasks to leaders and active members if necessary.
- Serve as a mentor and actively train incoming Minor Board members post-election and acceptance of the role.
- Maintain and update transitional guides to aid leadership transition between terms with the approval of the Executive Board.
- Work to create and promote an inclusive environment for all ages, colors, disabilities, gender identities or expressions, national origin, ethnicities/races, religions, sexes, sexual orientations, veteran statuses, and skill-levels.
- Aid in the resolution of conflict between officers, staff, and other members.
- Update the Executive Board about concerns and complaints made by members or event attendees.

##### Executive Board

###### President

- Must stay up to date on the happenings of all executive officers and committees by holding check-ins with each officer on a basis determined by the President at the beginning of the semester.
- Must communicate efficiently and effectively with Faculty Advisor at all times.
- Be the primary student representative to the college (including the University Program), community, and sponsors, as well as for regional and national organizations.
- Create a meeting agenda and share with the club prior to monthly General Body Meetings held by the President.
- Head and preside over all monthly General Body Meetings and Executive Board Meetings.
- Must regularly read each Secretary’s notes as well as remain up to date on each Committee’s affairs.
- Be in charge of the strategic planning and developmental tools of the club.
- Be the steward of an updated budget and financial plan and the development of the financial calendar.
- Help organize large events and aid in overseeing the events created by other area heads.
- Ensure that all other Officers maintain the Transitional Guide for their own position to aid with training and transitioning future Officers.
- Aid in the orientation and training of both new Executive and General members.

###### Treasurer

- Create, maintain, and upkeep a projected budget for all transactions and events for their term, with the budget being approved by at least two-thirds of the Executive Board.
- Approve all transactions made by the organization and keep a log of receipts and reasons for each transaction.
- Ensure all payments are made on time.
- Keep record of inventory for all objects in the club’s possession.
- Head and preside over weekly Financial Branch meetings.
- Collaborate with the Fundraising Chair to organize and manage fundraising events throughout the school year to benefit the club as a whole.
- Collaborate with the Director of Sponsorships with finding sponsors, working with them, and making sure all requirements are met for receiving the sponsorships.
- Strive to keep expenditures as minimal as possible, even in the event of excess capital.
- Work with the president on any dealings with federal agencies, including but not limited to the IRS, etc.
- Never use the Club’s money for personal use of themselves nor any of the members.
- Report improper use of club funds to Faculty Advisor.

###### Director of Public Relations & Diversity

- Head and preside over weekly Public Relations Branch meetings.
- Coordinate with Community and Competitive Directors and Managers to plan and publicize events.
- Oversee social media accounts, including but not limited to: Twitter, Instagram, Twitch, YouTube, etc.
- Ensure that the content calendar is created and communicated effectively between the Directors of Community and Competitive Affairs as well as the Social Media and Graphics Chairs.
- Monitor club email and communicate any important emails to necessary staff.
- Create, assign, and monitor monthly marketing tasks (such as flyer routes, tabling, etc.).
- Work with Social Media, Graphic Design, and Broadcast Chairs to create event flyers and promotional materials.
- Facilitate alumni relations and maintain alumni contact information for all graduated Buckeye Gaming Collective alumnus.
- Coordinate distribution of flyers around campus and in dormitories, obtaining approval when necessary.

###### Director of Competitive Affairs

- Head and preside over weekly Competitive Branch meetings.
- Create an inclusive environment and lead/welcome new players of the organization.
- Ensure that all competitive game managers/captains conduct tryouts in a timely, appropriate, and fair manner.
- Ensure that all competitive game managers/captains create a consistent practice schedule that is approved by the team in advance and that the schedule is followed.
- Ensure that all competitive teams are registered for competitions/leagues and aid team managers with the logistical aspects of playing in those competitions/leagues.
- Ensure that the club’s shared calendar is updated with all competitive events.
- Oversee members within their division of competitive gaming, including ensuring that all competitive game managers/captains and secretary accomplish their tasks.
- Ensure that the Director of Public Affairs and subsequent chairs are notified of current team activities.

###### Director of Community Management

- Head and preside over weekly Community Branch meetings.
- Create an inclusive environment and lead/welcome new members of the organization.
- Help organize events, fundraisers, etc. involving the club.
- Ensure room bookings and event materials are acquired in a timely manner.
- Work with community managers to plan at least one large scale event a semester.
- Work with community managers to program a series of smaller events throughout the semester, such as a game night series or a LAN series.
- Assist the treasurer in all sponsorship endeavors, as related to community events.
- Ensure that the Director of Public Affairs and subsequent chairs are notified of current events.

###### Director of Sponsorships

- Coordinate with President, Treasurer, and necessary Executive Board members to better understand sponsorships requests and needs.
- Coordinate directly with the Graphic Design Chair to procure all necessary files for graphics, broadcast and stream assets, and other marketing materials required for sponsorship deals.
- Consistently update the Buckeye Gaming Collective sponsorship packet for distribution to prospective sponsors and clients.
- Maintain a record of potential, contacted, previous, and current sponsors.
- Maintain good relations with previously established sponsors and clients.

###### All Members of the Executive Board Shall

- Adhere to all responsibilities and duties set forth by the Constitution.
- Be an undergraduate Voting Member of the Buckeye Gaming Collective.
- Attend Executive Board Meetings unless there is an excused absence, with Executive Board Members keeping each other accountable for meeting attendance.
- Communicate weekly in Branch Meetings with the Secretary, other relevant Executive Board Members, and Minor Board Members with updates and information.
- Delegate tasks to Minor Board and committee members.
- Serve as a mentor and actively train incoming Executive Board members post-voting and acceptance of role.
- Maintain and update transitional guides to aid leadership transition between terms.
- Work to create and promote an inclusive environment for all ages, colors, disabilities, gender identities or expressions, national origin, ethnicities/races, religions, sexes, sexual orientations, veteran statuses, and skill-levels.
- Aid in the resolution of conflict between officers, staff, and other members.
- Assist in monitoring Voting Member Status of all members in the club.

### Article III - Faculty Advisor

#### Section 1 - Faculty Advisor

This organization shall maintain a Faculty Advisor position to be held only by a faculty member or administrative professional staff member of The Ohio State University. If the Faculty Advisor’s position becomes vacant, a new Faculty Advisor will be appointed by a unanimous decision among the Executive Board.

#### Section 2 - Responsibilities

The general duties of the Faculty Advisor shall be to advise the Executive Board and organization when necessary. The Faculty Advisor shall serve as the final arbiter for any irresolvable disputes, and any approval not requiring a representative from The Ohio State University can be conducted through email.

### Article IV - Elections, and Method of Selecting and/or Removing Officers

#### Section 1 - Executive Board Elections and Method of Selecting Officers

The Executive Board elections are to be held once per academic school year, at least one month prior to the end of Spring Semester. Every Executive Board Officer position will be up for election and/or re-election, as every Executive Board term is one year. Executive Board Elections shall take place in several phases:

##### Nominations

The nominations window opens two weeks before elections are held and closes after one week of being open. Only Voting Members (see Article II for requirements) may nominate or be nominated for a position. Voting Members may nominate themselves and/or other members. Members who are nominated must graciously accept or respectfully decline the nomination before Elections Day, happening seven days after the nomination window closes.

##### Post Nominations

The list of nominated members will be sent out to Voting Members once the nominations window closes and will be updated to reflect acceptances and denials until Elections Day. Nominees may submit their resume before Elections Day, happening the week after the nominations window closes. Nominees interested in running for an Executive Board position must prepare a speech of no more than three minutes and be prepared to answer questions. Any resumes submitted will be made available to Voting Members.

##### Elections

Elections shall be run by the highest ranking Executive Board Members that is not running for a position. If all Executive Board Members are running for a position, the previous year’s president, highest ranking Executive Board Members, or Advisor shall run the election. Elections for positions will occur in the order listed in Article II Section 2. Election Day will run as follows:

###### Presentation

- At least 50% of Voting Members must be present to conduct Elections. If 50% cannot be reached within three attempts, then a mandatory absentee vote will occur.
- After moving to begin elections, nominees will be brought before the group one at a time, with all other nominees for the position standing outside of the room until their time to present. The order of nominees will be determined alphabetically by last name.
- Each nominee will present to the group on their speech. The presentation time will be agreed on by the group but is suggested to be three minutes, followed by a questions and answers session that lasts a maximum of five minutes.

###### Discussion

- Following all nominee speeches for a position, Voting Members will have time to civilly discuss platforms positives and concerns, some of which may include time management, experience, and so on.

###### Voting

- Votes will be done by secret ballot. On slips of paper, voters will write the name of their choice or “abstain,” meaning they do not choose any of the nominees. If a majority of present Voting Members has not been reached by one candidate, the top candidates who make up the majority of votes will have a runoff vote, where voters are told the top two and must choose between the two on another slip of paper.
- Once voting has concluded for a position, the Officer running the election shall announce the newly elected Executive Board Officer.

##### Trickle-down

When an Officer is successfully voted on, the remaining nominees will be asked if they would like to “trickle-down” to any of the next positions to be voted on.

##### Title Transfer

Once the Election has concluded, all current Executive Board members shall transfer their Officer titles to the newly elected Executive Board members. Each position is responsible for going through the appropriate Transition Guide and training the newly elected Officer in their position.

#### Section 2 - Minor Board Selection

Minor Board Selection will occur within one week of the conclusion of Executive Board Elections. Every Minor Board position will be up for selection and/or re-selection, as every Minor Board position term lasts until the new Executive Board is elected. Minor Board Selection shall take place in two phases:

##### Application

Online applications will be opened within a week of the conclusion of Executive Board Elections. Applications must include, at the minimum: name, Discord username, email, and resume and/or relevant experience.

##### Selection

Selection of all Minor Board positions, except for Team Managers, must be completed by the Executive Board within two weeks of the Minor Board Application deadline. Team Managers will be voted on by prior Team Members, with the candidate with majority votes receiving the position. If a majority of Team Players has not been reached by one candidate, the top candidates who make up the majority of votes will have a runoff vote, where voters are told the top two and must choose between the two. Selection results must be reported to the club once complete.

#### Section 3 - Method of Removing Officers

Should any member of the Executive Board be judged to be deficient in their duties, an appeal for evaluation may be made by a member of the Executive or Minor Board to the other members of the Executive Board and/or Faculty Advisor. The Executive Board and Minor Board of the club, assuming at least two-thirds are present for the vote, may remove the Officer from office upon reaching a two-thirds approval vote. Officers will then run a special election to fill that vacancy, as detailed in Article IV Section 5.

#### Section 4 - Method of Removing Minor Board Members

Should any member of the Minor Board be judged to be deficient in their duties, an appeal for evaluation may be made by their Director to the other members of the Executive Board. The Executive Board and Minor Board of the club, assuming at least two-thirds are present for the vote, may remove the person from office upon reaching a two-thirds approval vote. Officers may then appoint that position to another member of this organization or to temporarily create a vacancy in that position if no suitable replacement is found.

#### Section 5 - Executive Board Vacancies

Vacancies in leadership shall be filled as soon as possible by a special election under the same procedure of normal elections, should an Officer resign or be removed during the school year. If an Officer decides to resign during the year, they must notify the rest of the Executive Board with two weeks and aid in the selection and transition of the member to take their position.

### Article V - Meetings

#### Section 1 - General Body Meetings

General Body Meetings will primarily be called monthly by the President. In the case that the President is unable to hold the meeting, meetings shall be run by the next highest ranking officer as ranked in Article Article II, Section 2. The set dates and times for each meeting shall be established at the beginning of each semester.

#### Section 2 - Branch Meetings

The four branches of the Club are: Competitive, Community, Public Relations, and Financial. Branch Meetings, consisting of the Director, related Minor Board positions, and Minor Board committees, are to be held weekly, at a minimum, for each Branch. Other Branch Meetings will be run and scheduled separately by their respective Directors but should be communicated to the Executive Board.

#### Section 3 - Executive Board Meetings

Executive Board Meetings, consisting of the Executive Board and any other positions as decided by the President, are to be hosted every other week, at the minimum.

#### Section 4 - Special Meetings

Special Meetings may be called at any time by the President and should include all members of the Executive Board and Minor Board.  Special Meetings should only be called on urgent whole-club measures, including but not limited to Executive Board impeachment, a financial crisis, or amendment voting.

### Article VI - Amendments

#### Section 1 - Proposal

Proposed amendments should be in writing and read during the Executive Board Meeting in which they are proposed. They should be read again at the subsequent meeting and then voted upon by Executive Board and Minor Board. Proposals should be made available to members for review following the meeting in which they are proposed.

#### Section 2 - Voting Requirements

This Constitution can only be amended by two-thirds approval among the Executive Board and Minor Board, granted that at least two-thirds of the Executive Board and Minor Board are present to vote. If a quorum, defined by two-thirds, cannot be reached within three consecutive Executive Board Meetings, then there will be a mandatory absentee vote.

#### Section 3 - Amendments Made

Below, the amendments made to this Constitution shall be listed.
