---
layout: default
title: About
---

## About

The purpose of this organization shall be to provide an engaging environment for both students and community members to participate in recreational video game and entertainment related activities. An additional cause has been added to support the interest and retention of students (especially from underprivileged areas in Columbus) that are entering into STEMM fields (Science, Technology, Engineering, Math and Medicine). The purposes for which this organization is formed, and any associated assets raised, are exclusively for the betterment of the community and not for the private gain of any person.

## Organization Structure

<img src="{{ '/assets/images/other/structure_2020-2021.png' | prepend: site.url }}" class="w-100" alt="Current OSU BGC organization structure.">

<!--
include components/org-structure.html
-->
