# osubgc.gitlab.io

This is the demo website for the Buckeye Gaming Collective, primarily used for
pulling information on screen in near real time for live broadcasts.

## License

- Content for osubgc.gitlab.io is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
- Code for osubgc.gitlab.io is licensed under a [GNU Affero General Public License v3](http://www.gnu.org/licenses/agpl-3.0.en.html).

## Contributing

### Results

Results are pulled from Google Sheets by running `./scripts/download-current-results.sh`.  This will place the current official OSU BGC results at `_data/scores.csv`.

To recreate the data or to manually test, the `csv` should have the following header:

```csv
"Timestamp","state","game","league","date","time","home_name","home_score","away_name","away_score","broadcast_link","ical_date_start","ical_date_end"
```

### Games

_Work in progress. Additional documentation coming soon._

### Leagues

_Work in progress. Additional documentation coming soon._
